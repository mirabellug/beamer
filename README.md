# Sujet

Présentation et atelier d'introduction à beamer.

# Compilation

À compiler avec XeLaTeX et l'option --shell-escape (environnement minted).

# Crédit

Cercle chromatique fait par freetux, CC By 4.0
https://git.la-bibliotex.fr/nicolas/Cercle_chromatique/
