# Schémas de couleurs (Cercle chromatique)

  * Monochromatique. Même rayon. 2-n couleurs. Contraste faible.
  * Complémentaires. 180°. 2 Couleurs, Contraste maximal.
  * Analogues. Adj./Teintes proches. 3 Couleurs. Contraste limité.
  * Complémentaires adjacentes. 1 couleur + 2 adj. à la complémentaire. Contraste important /1 couleurs
  * Triade. 120°. 3 Couleurs. Contraste moyen.
  * Complémentaires double rectangle. 2 complément. à 60°. 4 couleurs. Fort contraste 2 à 2.
  * Complémentaires double carré. 2 complément. à 90°. 4 couleurs. Fort contraste 2 à 2.
